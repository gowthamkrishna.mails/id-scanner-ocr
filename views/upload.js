function readURL(input) {
if (input.files && input.files[0]) {

  var reader = new FileReader();

  reader.onload = function(e) {
    $('.image-upload-wrap').hide();

    $('.file-upload-image').attr('src', e.target.result);
    $('.file-upload-content').show();

    $('.image-title').html(input.files[0].name);
  };

  reader.readAsDataURL(input.files[0]);

}
}

{/* <script>
    $("#submitBtn").click(function (){
        var fd = new FormData();
        fd.append('fileType', $("#fileType").val());
        var file = $("#fileUpl")[0].files[0];
        fd.append("file", file);
        console.log("formdata: "+fd);
        console.log("file:"+file.name);
        $.ajax(
            {
                type: "post",
                url: "/upload",
                data : fd,
                processData : false,
                contentType : false,
                success: function(data1){
                    document.getElementById("ocrData").innerHTML = data1;
                }
            }
        );
    });

</script> */}
