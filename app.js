

const express =require("express");
//const bodyParser = require("body-parser");
const app=express();
const fs=require("fs");
const multer=require("multer");
const ejs=require('ejs');
const path=require('path');
const {body, validationResult}=require('express-validator');
const { createWorker } = require('tesseract.js');
const { title } = require("process");


const worker = createWorker({
    logger: m => console.log(m)
});

const storage=multer.diskStorage({
    destination: (req,file,cb)=>{
        cb(null, "./uploads")
    },
    filename:(req,file,cb)=>{
        cb(null,file.originalname)
    }
});
 app.use(express.json());
 app.use(express.urlencoded({extended: true}));
const upload =multer({storage}).single("avatar");

app.set("view engine","ejs");
const urlEncodedParser=express.urlencoded({extended: true})
app.get('/',(req,res)=>
    res.render('index'))
    
    app.post('/getJson', function (req, res) {
        console.log(req.body.example);
     });
    

// Defined API for handle all requests comes on /upload route (or from index's submit btn click)
app.post('/upload', urlEncodedParser, function(req, res)  {
    //res.json(req.body);
    console.log("body--\n"+ JSON.stringify(req.body));

    // Stored file into upload directory
    //  let upload = multer({
    //      storage: storage
    //  })
    

    upload(req, res, err => {

        // Reading uploaded file from upload directory
        fs.readFile(`./uploads/${req.file.originalname}`, (err, data) => {

            // Displaying error if anything goes wrong 
            if(err) return console.error("this is error", err);

             // Self execution function to use async await 
              (async () => {
                // Tesseract worker loaded with langague option
                try {
                await worker.load();
                await worker.loadLanguage('eng');
                await worker.initialize('eng');

                // Document extraction by recognize method of Tesseract and console result
                const { data: { text } } = await worker.recognize(data);
                
                    res.send(aadhar(text,res));
                    //   res.send(license(text,res));
                // res.send(Pan(text,res));
                
                // Respond send to view with result text and terminated worker after process complete
                await worker.terminate();
                
                }catch(e){
                    console.log("---------------------------------\nError:\n"+e+"\n---------------------------------")
                }
              })();
        })
    })
})
const PORT=3000||process.env.PORT;
app.listen(PORT,()=>console.log('hey iam running on port', PORT));


function aadhar(text,res){
    var newText = text.split("\n")[3];
    var questionText = newText.replace(/[0-9]/g, '');
    var result = "<p style='background-color:powderblue;display:block;'>Name :</h1>"+ questionText;
    // ------------------------------dob---------------------------//
    
     result = result +"<br>DOB :"+getDate(text);
     var regex=/male$ /gim;
     var gen=regex.test(text);
     // console.log(gen);
     var val
      if(gen){
        val="Male"
     //      var Gender="Male"
      }else {
           val="Female"
      }

    result = result +"<br>Gender :"+val;
   console.log("working");
    result = result +"<br>uid : Valid";
    return result;
}

 function getDate(text)
 {
     var day, month, year;
 
     result = text.match("[0-9]{2}([\-/ \.])[0-9]{2}[\-/ \.][0-9]{4}");
     if(null != result) {
         dateSplitted = result[0].split(result[1]);
         day = dateSplitted[0];
         month = dateSplitted[1];
         year = dateSplitted[2];
     }
     result = text.match("[0-9]{4}([\-/ \.])[0-9]{2}[\-/ \.][0-9]{2}");
     if(null != result) {
         dateSplitted = result[0].split(result[1]);
         day = dateSplitted[2];
         month = dateSplitted[1];
         year = dateSplitted[0];
     }
 
     if(month>12) {
         aux = day;
         day = month;
         month = aux;
     }
 
     return year+"/"+month+"/"+day;
 }
 function getDate1(text)
 {
     var day, month, year;
     var text1=text.split("\n")[7];
     result = text1.match("[0-9]{2}([\-/ \.])[0-9]{2}[\-/ \.][0-9]{4}");
     if(null != result) {
         dateSplitted = result[0].split(result[1]);
         day = dateSplitted[0];
         month = dateSplitted[1];
         year = dateSplitted[2];
     }

     result = text.match("[0-9]{4}([\-/ \.])[0-9]{2}[\-/ \.][0-9]{2}");
     if(null != result) {
         dateSplitted = result[0].split(result[1]);
         day = dateSplitted[2];
         month = dateSplitted[1];
         year = dateSplitted[0];
     }
 
     if(month>12) {
         aux = day;
         day = month;
         month = aux;
     }
 
     return year+"/"+month+"/"+day;
 }

 function license(text,res){
    //  title="<h1>Driving Licence</h1>";
     
    var newText = text.split("\n")[9];
    
    var questionText = newText.replace(/~¢,§ o=/gm, '');
    result="<p style='background-color:powderblue;display:block;'>Name :</h1>"+questionText+"<br>";
    // ---------??----------??
    
    result=result+"DOI :"+getDate(text)+"<br>";
    result=result+"DOB :"+getDate1(text)+"<br>";
    return "<br><br><br><br>"+result;
    
 }
 function getDate2(text)
 {
     var day, month, year;
     var text2=text.split("\n")[7];
     result = text1.match("[0-9]{2}([\-/ \.])[0-9]{2}[\-/ \.][0-9]{4}");
     if(null != result) {
         dateSplitted = result[0].split(result[1]);
         day = dateSplitted[0];
         month = dateSplitted[1];
         year = dateSplitted[2];
     }

     result = text.match("[0-9]{4}([\-/ \.])[0-9]{2}[\-/ \.][0-9]{2}");
     if(null != result) {
         dateSplitted = result[0].split(result[1]);
         day = dateSplitted[2];
         month = dateSplitted[1];
         year = dateSplitted[0];
     }
 
     if(month>12) {
         aux = day;
         day = month;
         month = aux;
     }
 
     return year+"/"+month+"/"+day;
 }

 function Pan(text,res){
    var newText = text.split("\n")[2];
    
    var questionText = newText.replace(/~¢,§ o=/gm, '');
    result="<p style='background-color:powderblue;display:block;'>Name :</h1>"+questionText+"<br>";
    result=result+"DOB :"+getDate(text);
    var newText = text.split("\n")[6];
       result  =result+"<br>PAN Number :"+newText;
       return result;
 }